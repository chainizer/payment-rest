import {expect} from 'chai';
import 'mocha';
import '../src/main';
import {getHooks, getServer} from '@chainizer/support-app';
import {getCnqAmqp} from '@chainizer/support-cnq';

const wait = (ms) => (new Promise(resolve => setTimeout(resolve, ms)));

describe('app', () => {

    it('should check health and stop the app', async () => {
        await wait(100);
        let healthError;
        try {
            getHooks().health.forEach(async hook => await hook());
        } catch (e) {
            healthError = e;
        }

        const amqp = await getCnqAmqp();
        await amqp.channel.checkQueue(amqp.replyToQueue);

        await wait(100);

        let shutdownError;
        try {
            getHooks().shutdown.forEach(async hook => await hook());
        } catch (e) {
            shutdownError = e;
        }

        getServer().close();
        expect(healthError, 'health hooks failed').to.not.exist;
        expect(shutdownError, 'shutdown hooks failed').to.not.exist;
    });

});

