import {logger} from '@chainizer/support-winston';
import {startApp} from '@chainizer/support-app';
import '@chainizer/support-cnq';
import './commands';
import './queries';

startApp().catch(error => logger().error('App unable to start!', error));
