#!/usr/bin/env bash

# RABBIT

RABBIT_URL="amqp://"
if [ ! -z ${RABBIT_PASS} ]; then
    RABBIT_URL="${RABBIT_URL}${RABBIT_USER}:${RABBIT_PASS}@"
fi
RABBIT_URL="${RABBIT_URL}${RABBIT_HOST:-rabbit-discovery}/"

# START

ARGS="$@ --set.cnq.amqp.connection ${RABBIT_URL} --set.crypto.salt ${CRYPTO_SALT}"

exec npm start --production -- ${ARGS}
