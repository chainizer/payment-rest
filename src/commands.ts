import {
    CREATE_PAYMENT,
    CreatePaymentCmd,
    CreatePaymentCmdRes,
    NotAuthorized,
    WITHDRAW_PAYMENT,
    WithdrawPaymentCmd,
    WithdrawPaymentCmdRes
} from '@chainizer/payment-api';
import {Metadata, registerTask, TaskType} from '@chainizer/support-executor';
import * as moment from 'moment';
import {v4 as uuid} from 'uuid';
import {config} from './config';
import {decrypt, encrypt, hash} from './crypto';
import {checkPayment, createAccount, fetchPayment, insertPayment, withdrawAccount} from './tasks';
import DurationConstructor = moment.unitOfTime.DurationConstructor;

registerTask(TaskType.command, CREATE_PAYMENT, async (payload: CreatePaymentCmd, metadata: Metadata): Promise<CreatePaymentCmdRes> => {
    const token = uuid();
    const {privateKey, address} = await createAccount(metadata.messageId, payload.coin);
    const encryptedPrivateKey = encrypt(privateKey, token);
    const challenge = hash(token);
    const coin = payload.coin;
    const timeToLive = moment().add(
        config().business.checkingTTL.duration,
        <DurationConstructor> config().business.checkingTTL.unit
    ).toISOString();
    await insertPayment(metadata.messageId, {
        coin,
        address,
        encryptedPrivateKey,
        challenge,
        callbackUrl: payload.callbackUrl
    });
    await checkPayment(metadata.messageId, {
        coin, address, challenge, timeToLive
    });
    return {address, token};
});

registerTask(TaskType.command, WITHDRAW_PAYMENT, async (payload: WithdrawPaymentCmd, metadata: Metadata): Promise<WithdrawPaymentCmdRes> => {
    const token = (metadata.headers.authorization || '').replace(/bearer/i, '').trim();
    if (!token) {
        throw new NotAuthorized();
    }
    const challenge = hash(token);
    const payment = await fetchPayment(metadata.messageId, challenge, {
        coin: payload.coin,
        address: payload.address
    });
    if (!payment) {
        throw new NotAuthorized();
    }
    return await withdrawAccount(metadata.messageId, payload.coin, {
        to: payload.to,
        message: payload.message,
        privateKey: decrypt(payment.encryptedPrivateKey, token)
    });
});
