import {Coin, WITHDRAW_PAYMENT, WithdrawPaymentCmd, WithdrawPaymentCmdRes} from '@chainizer/payment-api';
import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {expect} from 'chai';
import 'mocha';
import '../src/commands';
import {FakeCnq} from '@chainizer/support-cnq-test';
import {encrypt} from '../src/crypto';

describe('withdraw-payment', () => {
    let f = new FakeCnq();

    beforeEach(() => {
        f.reset();
    });

    it('should withdraw a payment', async () => {
        f.handle(
            0,
            (exchange, routingKey, bContent) => {
                expect(exchange).to.be.eq('payment.repository');
                expect(routingKey).to.be.eq('query');
                const content = JSON.parse(bContent.toString());
                expect(content).to.have.nested.property('coin');
                expect(content).to.have.nested.property('address');
            },
            () => new Buffer(JSON.stringify({encryptedPrivateKey: encrypt('encryptedPrivateKey', '1234')}))
        );
        f.handle(
            1,
            (exchange, routingKey, bContent) => {
                expect(exchange).to.be.eq('payment.ethereum');
                expect(routingKey).to.be.eq('command');
                const content = JSON.parse(bContent.toString());
                expect(content).to.have.nested.property('to');
                expect(content).to.have.nested.property('message');
                expect(content).to.have.nested.property('privateKey');
            },
            () => new Buffer(JSON.stringify({feeTx: 'feeTx', userTx: 'userTx'}))
        );

        const payload: WithdrawPaymentCmd = {
            coin: Coin.ethereum,
            address: 'address',
            message: 'message',
            to: 'to'
        };
        const metadata: Metadata = {
            messageId: 'test',
            headers: {
                'authorization': 'bearer 1234'
            }
        };
        const r = <WithdrawPaymentCmdRes> await executeTask(TaskType.command, WITHDRAW_PAYMENT, payload, metadata);
        expect(r).to.exist;
        expect(r).to.have.property('userTx', 'userTx');
        expect(r).to.have.property('feeTx', 'feeTx');
    });

});
