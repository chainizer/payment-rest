import {GET_PAYMENT, GetPaymentQry, GetPaymentQryRes, NotAuthorized} from '@chainizer/payment-api';
import {Metadata, registerTask, TaskType} from '@chainizer/support-executor';
import {merge, pick} from 'lodash';
import {hash} from './crypto';
import {fetchPayment, getBalance} from './tasks';

registerTask(TaskType.query, GET_PAYMENT, async (payload: GetPaymentQry, metadata: Metadata): Promise<GetPaymentQryRes> => {
    const token = (metadata.headers.authorization || '').replace(/bearer/i, '').trim();
    if (!token) {
        throw new NotAuthorized();
    }
    const challenge = hash(token);
    const payment = await fetchPayment(metadata.messageId, challenge, {
        coin: payload.coin,
        address: payload.address
    });
    if (!payment) {
        throw new NotAuthorized();
    }
    const balance = await getBalance(metadata.messageId, payload.coin, payload.address);
    return merge(pick(payment, 'coin', 'address', 'callbackUrl', 'events'), {balance});
});
