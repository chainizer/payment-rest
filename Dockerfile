FROM resin/i386-alpine-node:10.4

#RUN [ "cross-build-start" ]

RUN apk add --update git make g++ bash

COPY ./*.json /opt/
COPY ./docker /
COPY ./lib /opt/lib
COPY ./config /opt/config

WORKDIR /opt

RUN npm install --production

#RUN [ "cross-build-end" ]

EXPOSE 3000

ENTRYPOINT ["./entrypoint.sh"]
