import {Coin, GET_PAYMENT, GetPaymentQry, GetPaymentQryRes} from '@chainizer/payment-api';
import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {expect} from 'chai';
import 'mocha';
import '../src/queries';
import {FakeCnq} from '@chainizer/support-cnq-test';
import {encrypt} from '../src/crypto';

describe('get-payment', () => {
    let f = new FakeCnq();

    beforeEach(() => {
        f.reset();
    });

    it('should get a payment', async () => {
        f.handle(
            0,
            (exchange, routingKey, bContent) => {
                expect(exchange).to.be.eq('payment.repository');
                expect(routingKey).to.be.eq('query');
                const content = JSON.parse(bContent.toString());
                expect(content).to.have.nested.property('coin');
                expect(content).to.have.nested.property('address');
            },
            () => new Buffer(JSON.stringify({
                address: 'address',
                encryptedPrivateKey: encrypt('encryptedPrivateKey', '1234')
            }))
        );
        f.handle(
            1,
            (exchange, routingKey, bContent) => {
                expect(exchange).to.be.eq('payment.ethereum');
                expect(routingKey).to.be.eq('query');
                const content = JSON.parse(bContent.toString());
                expect(content).to.have.nested.property('address');
            },
            () => new Buffer(JSON.stringify({balance: '0'}))
        );

        const payload: GetPaymentQry = {
            coin: Coin.ethereum,
            address: 'address'
        };
        const metadata: Metadata = {
            messageId: 'test',
            headers: {
                'authorization': 'bearer 1234'
            }
        };
        const r = <GetPaymentQryRes> await executeTask(TaskType.query, GET_PAYMENT, payload, metadata);
        expect(r).to.exist;
        expect(r).to.have.not.nested.property('encryptedPrivateKey');
        expect(r).to.have.nested.property('address');
        expect(r).to.have.nested.property('balance');
    });

});
