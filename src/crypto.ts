import {createCipher, createDecipher, createHash} from 'crypto';
import {config} from './config';

export function encrypt(input: string, password: string): string {
    const cipher = createCipher(
        config().crypto.algorithm.encryption,
        password
    );
    return cipher.update(input, 'utf8', 'hex') + cipher.final('hex');
}

export function decrypt(input: string, password: string): string {
    const decipher = createDecipher(
        config().crypto.algorithm.encryption,
        password
    );
    return decipher.update(input, 'hex', 'utf8') + decipher.final('utf8');
}

export function hash(value: string): string {
    const hash = createHash(config().crypto.algorithm.hashing);
    hash.update(config().crypto.salt);
    hash.update(value);
    return hash.digest('hex').toString();
}
