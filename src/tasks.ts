import {v4 as uuid} from 'uuid';
import {
    CHECK_PAYMENT,
    CheckPaymentCmd,
    CREATE_ACCOUNT,
    CreateAccountCmd,
    CreateAccountCmdRes,
    FETCH_PAYMENT,
    FetchPaymentQry,
    FetchPaymentQryRes,
    GET_BALANCE,
    GetBalanceQry,
    GetBalanceQryRes,
    INSERT_PAYMENT,
    InsertPaymentCmd,
    WITHDRAW_ACCOUNT,
    WithdrawAccountCmd,
    WithdrawAccountCmdRes
} from '@chainizer/payment-api';
import {Metadata, TaskType} from '@chainizer/support-executor';
import {dispatchTask} from '@chainizer/support-cnq';

export async function createAccount(transactionId: string, coin: string) {
    const payload: CreateAccountCmd = {};
    const metadata = {
        messageId: uuid(),
        transactionId
    };
    const {privateKey, address} = <CreateAccountCmdRes> await dispatchTask(coin, TaskType.command, CREATE_ACCOUNT, payload, metadata);
    return {privateKey, address};
}

export async function insertPayment(transactionId: string, payload: InsertPaymentCmd) {
    const metadata = {
        messageId: uuid(),
        transactionId
    };
    await dispatchTask('repository', TaskType.command, INSERT_PAYMENT, payload, metadata);
}

export async function checkPayment(transactionId: string, payload: CheckPaymentCmd) {
    const metadata = {
        messageId: uuid(),
        transactionId
    };
    await dispatchTask('checker', TaskType.command, CHECK_PAYMENT, payload, metadata, false);
}

export async function fetchPayment(transactionId: string, challenge: string, payload: FetchPaymentQry): Promise<FetchPaymentQryRes> {
    const metadata = {
        messageId: uuid(),
        transactionId,
        challenge
    };
    return await dispatchTask('repository', TaskType.query, FETCH_PAYMENT, payload, metadata);
}

export async function withdrawAccount(transactionId: string, coin: string, payload: WithdrawAccountCmd): Promise<WithdrawAccountCmdRes> {
    const metadata = {
        messageId: uuid(),
        transactionId
    };
    return await dispatchTask(coin, TaskType.command, WITHDRAW_ACCOUNT, payload, metadata);
}

export async function getBalance(transactionId: string, coin: string, address: string): Promise<string> {
    const payload: GetBalanceQry = {
        address
    };
    const metadata: Metadata = {
        messageId: uuid(),
        transactionId
    };
    const response = <GetBalanceQryRes> await dispatchTask(coin, TaskType.query, GET_BALANCE, payload, metadata)
    return response.balance;
}
