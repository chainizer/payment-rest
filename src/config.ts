import {Config, config as oConfig} from '@chainizer/support-config';

export interface CustomConfig extends Config {
    crypto: {
        algorithm: {
            hashing: string
            encryption: string
        }
        salt: string
    }
    business: {
        checkingTTL: {
            duration: number
            unit: string
        }
    }
}

export function config(argv?: string | string[]): CustomConfig {
    return <CustomConfig> oConfig({argv});
}
