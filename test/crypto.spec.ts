import {expect} from 'chai';
import 'mocha';
import {decrypt, encrypt, hash} from '../src/crypto';

describe('crypto', () => {

    it('should hash', async () => {
        const hashedValue = hash('test');
        expect(hashedValue).to.exist;
        expect(hashedValue).to.be.equals('77d1d1116b9a44eff0c29f5b289e269fe5da39cd28ff8bc0daf63824aaa6d0971b569575bfd5dbea53f248e45b0935a964efce95d2a3a64a5a397da8e25d88e6');
    });

    it('should encrypt and decrypt', async () => {
        const input = 'input';
        const password = 'password';
        const encryptedInput = encrypt(input, password);
        const decryptedInput = decrypt(encryptedInput, password);
        expect(decryptedInput).to.be.equals(input);
    });

});
