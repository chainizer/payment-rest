import {Coin, CREATE_PAYMENT, CreatePaymentCmd, CreatePaymentCmdRes} from '@chainizer/payment-api';
import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {expect} from 'chai';
import 'mocha';
import '../src/commands';
import {FakeCnq} from '@chainizer/support-cnq-test';

describe('create-payment', () => {
    let f = new FakeCnq();

    beforeEach(() => {
        f.reset();
    });

    it('should created a payment', async () => {
        f.handle(
            0,
            (exchange, routingKey, bContent) => {
                expect(exchange).to.be.eq('payment.ethereum');
                expect(routingKey).to.be.eq('command');
                const content = JSON.parse(bContent.toString());
                expect(Object.keys(content)).to.have.lengthOf(0);
            },
            () => new Buffer(JSON.stringify({privateKey: 'privateKey', address: 'address'}))
        );
        f.handle(
            1,
            (exchange, routingKey, bContent) => {
                expect(exchange).to.be.eq('payment.repository');
                expect(routingKey).to.be.eq('command');
                const content = JSON.parse(bContent.toString());
                expect(content).to.have.nested.property('coin');
                expect(content).to.have.nested.property('address');
                expect(content).to.have.nested.property('encryptedPrivateKey');
                expect(content).to.have.nested.property('challenge');
                expect(content).to.have.nested.property('callbackUrl');
            },
            () => new Buffer('')
        );
        f.handle(
            2,
            (exchange, routingKey, bContent) => {
                expect(exchange).to.be.eq('payment.checker');
                expect(routingKey).to.be.eq('command');
                const content = JSON.parse(bContent.toString());
                expect(content).to.have.nested.property('coin');
                expect(content).to.have.nested.property('address');
                expect(content).to.have.nested.property('challenge');
                expect(content).to.have.nested.property('timeToLive');
            }
        );

        const payload: CreatePaymentCmd = {
            coin: Coin.ethereum,
            callbackUrl: 'https://payment.chainizer.com'
        };
        const metadata: Metadata = {
            messageId: 'test'
        };
        const r = <CreatePaymentCmdRes> await executeTask(TaskType.command, CREATE_PAYMENT, payload, metadata);
        expect(r).to.exist;
        expect(r).to.have.property('token');
        expect(r).to.have.property('address', 'address');
    });

});
